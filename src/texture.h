#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include <vector>
#include <memory>

class Texture
{
public:
    typedef std::shared_ptr<Texture> Ptr;

public:
    Texture(unsigned int width, unsigned int height, const std::vector<unsigned char>& data);
    ~Texture() { glDeleteTextures(1, &m_id); }

    GLuint id() const { return m_id; }

    GLvoid bind(int index = 0) const {
        this->active(index);
        glBindTexture(GL_TEXTURE_2D, m_id);
    }

    GLvoid unbind() const {
        glBindTexture(GL_TEXTURE_2D, m_id);
    }

    GLvoid active(int index) const {
        glActiveTexture(GL_TEXTURE0 + index);
    }

private:
    void generate();

private:
    Texture(const Texture& other) = delete;
    Texture& operator=(const Texture& other) = delete;

private:
    GLuint m_id;
    unsigned int m_width;
    unsigned int m_height;
    std::vector<unsigned char> m_data;
};

#endif // TEXTURE_H
