#ifndef SHADER_H
#define SHADER_H

#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
public:
    Shader(const std::string& vertexPath, const std::string& fragmentPath);

    ~Shader() {
        stop();
        glDeleteProgram(m_program);
    }

    void start() {
        glUseProgram(m_program);
    }

    void stop() {
        glUseProgram(0);
    }

    void loadTexture(const std::string& name, GLuint index)
    {
        glUniform1i(glGetUniformLocation(m_program, name.c_str()), index);
    }

    void loadModelMatrix(const glm::mat4& model) {
        glUniformMatrix4fv(glGetUniformLocation(m_program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    }

    void loadViewMatrix(const glm::mat4& view) {
        glUniformMatrix4fv(glGetUniformLocation(m_program, "view"), 1, GL_FALSE, glm::value_ptr(view));
    }

    void loadProjectionMatrix(const glm::mat4& projection) {
        glUniformMatrix4fv(glGetUniformLocation(m_program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
    }

private:
    /* Delete the copy constructor and assignment functions. */
    Shader(const Shader& other) = delete;
    Shader& operator=(const Shader& other) = delete;

private:
    GLuint m_program;
};

#endif // SHADER_H
