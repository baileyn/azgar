#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;

#define WINDOW_TITLE "Game"
#define RESOURCE_DIRECTORY "resources/"

class Game
{
public:
    Game();
    ~Game();

    void loop();

private:
    Game(const Game& other) = delete;
    Game& operator=(const Game& other) = delete;

private:
    GLFWwindow* m_window;
};

#endif // GAME_H
