﻿#include <iostream>
#include <vector>

#include "game.h"

#include "shader.h"
#include "texture.h"
#include "resourceloader.h"

#include <stdio.h>

int main()
{
    printf("start\n");
    Game game;
    game.loop();

    return EXIT_SUCCESS;
}

