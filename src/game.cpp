#include "game.h"

#include <string>
#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <array>

#include "shader.h"
#include "texture.h"
#include "resourceloader.h"

std::array<bool, 2014> keys;

static std::vector<GLfloat> vertices {
        -0.5f,0.5f,-0.5f, 0, 0,
        -0.5f,-0.5f,-0.5f, 0, 1,
        0.5f,-0.5f,-0.5f, 1, 1,
        0.5f,0.5f,-0.5f, 1, 0,

        -0.5f,0.5f,0.5f, 0, 0,
        -0.5f,-0.5f,0.5f, 0, 1,
        0.5f,-0.5f,0.5f, 1, 1,
        0.5f,0.5f,0.5f, 1, 0,

        0.5f,0.5f,-0.5f, 0, 0,
        0.5f,-0.5f,-0.5f, 0, 1,
        0.5f,-0.5f,0.5f, 1, 1,
        0.5f,0.5f,0.5f, 1, 0,

        -0.5f,0.5f,-0.5f, 0, 0,
        -0.5f,-0.5f,-0.5f, 0, 1,
        -0.5f,-0.5f,0.5f, 1, 1,
        -0.5f,0.5f,0.5f, 1, 0,

        -0.5f,0.5f,0.5f, 0, 0,
        -0.5f,0.5f,-0.5f, 0, 1,
        0.5f,0.5f,-0.5f, 1, 1,
        0.5f,0.5f,0.5f, 1, 0,

        -0.5f,-0.5f,0.5f, 0, 0,
        -0.5f,-0.5f,-0.5f, 0, 1,
        0.5f,-0.5f,-0.5f, 1, 1,
        0.5f,-0.5f,0.5f, 1, 0,

};

static std::vector<GLuint> indices {
        0,1,3,
        3,1,2,
        4,5,7,
        7,5,6,
        8,9,11,
        11,9,10,
        12,13,15,
        15,13,14,
        16,17,19,
        19,17,18,
        20,21,23,
        23,21,22

};

glm::vec3 cameraPos = glm::vec3{0.0f, 3.0f, 3.0f};
glm::vec3 cameraFront = glm::vec3{0.0f, 0.0f, -1.0f};
glm::vec3 cameraUp = glm::vec3{0.0f, 1.0f, 0.0f};

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    keys[key] = action != GLFW_RELEASE;
}

void do_movement(GLfloat deltaTime)
{

    static GLfloat cameraSpeed = 5.0f * deltaTime;

    if(keys[GLFW_KEY_W]) {
        cameraPos += cameraSpeed * cameraFront;
    }

    if(keys[GLFW_KEY_S]) {
        cameraPos -= cameraSpeed * cameraFront;
    }

    if(keys[GLFW_KEY_A]) {
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }

    if(keys[GLFW_KEY_D]) {
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }
}

Game::Game()
{
    if(!glfwInit()) {
        std::cerr << "Failed to initialize GLFW\n";
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; shouldn't be needed.
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL.
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    m_window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, NULL, NULL);
    if(m_window == nullptr) {
        std::cerr << "Failed to open GLFW window.\n";
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(m_window, key_callback);

    glfwMakeContextCurrent(m_window);

    glewExperimental = true; // Needed in core profile.
    if(glewInit() != GLEW_OK) {
        std::cerr << "Failed to initialize GLEW\n";
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
}

Game::~Game()
{
    glfwTerminate();
}

void Game::loop()
{
    Shader shader{RESOURCE_DIRECTORY "vert.glsl", RESOURCE_DIRECTORY "frag.glsl"};

    GLuint vao{}, vbo{}, ebo{};
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), static_cast<GLvoid*>(0));
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);
    glBindVertexArray(0);

    ResourceLoader loader{"resources/"};

    Texture::Ptr texture = loader.loadTexture("image.png");

    glm::mat4 projection = glm::perspective(70.0f, static_cast<float>(WINDOW_WIDTH) / static_cast<float>(WINDOW_HEIGHT), 0.1f, 100.0f);

    std::vector<glm::vec3> cubePositions {
      glm::vec3( 0.0f,  0.0f,  0.0f),
      glm::vec3( 2.0f,  5.0f, -15.0f),
      glm::vec3(-1.5f, -2.2f, -2.5f),
      glm::vec3(-3.8f, -2.0f, -12.3f),
      glm::vec3( 2.4f, -0.4f, -3.5f),
      glm::vec3(-1.7f,  3.0f, -7.5f),
      glm::vec3( 1.3f, -2.0f, -2.5f),
      glm::vec3( 1.5f,  2.0f, -2.5f),
      glm::vec3( 1.5f,  0.2f, -1.5f),
      glm::vec3(-1.3f,  1.0f, -1.5f)
    };

    glEnable(GL_DEPTH_TEST);

    GLfloat delta = 0.0f;
    GLfloat lastFrame = 0.0f;

    do {
        GLfloat currentFrame = glfwGetTime();
        delta = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // Poll for events.
        glfwPollEvents();

        do_movement(delta);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 view = glm::lookAt(
                    cameraPos,
                    cameraPos + cameraFront,
                    cameraUp
                    );

        glBindVertexArray(vao);
        shader.start();

        shader.loadViewMatrix(view);
        shader.loadProjectionMatrix(projection);

        shader.loadTexture("background", 0);
        texture->bind();
        for(auto i : cubePositions) {
            glm::mat4 model{1.0f};
            model = glm::translate(model, i);
            model = glm::rotate(model, delta * glm::radians(90.0f), glm::vec3{0.0f, 1.0f, 1.0f});
            model = glm::scale(model, glm::vec3{1.0f, 1.0f, 1.0f});

            shader.loadModelMatrix(model);

            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
        }
        texture->unbind();

        shader.stop();
        glBindVertexArray(0);

        // Swap buffers
        glfwSwapBuffers(m_window);
    } while(glfwWindowShouldClose(m_window) == GL_FALSE);

    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    //glDeleteBuffers(1, &ebo);
}

