#include "resourceloader.h"

#include <memory>

#include <lodepng.h>

#include <stdio.h>

ResourceLoader::ResourceLoader(const std::string& resourceDirectory)
    : m_resourceDirectory{resourceDirectory}
{

}

Texture::Ptr ResourceLoader::loadTexture(const std::string& file) const
{
    std::vector<unsigned char> image;
    std::vector<unsigned char> flippedImage;
    unsigned int width{}, height{};
    unsigned int error = lodepng::decode(image, width, height, m_resourceDirectory + file);

    if(error) {
        printf("deocder error %d: %s\n", error, lodepng_error_text(error));

        return nullptr;
    }

    return std::make_shared<Texture>(width, height, image);
}
