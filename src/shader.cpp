#include "shader.h"

#include <fstream>
#include <iostream>

static std::string readLines(const std::string& path)
{
    std::ifstream s{path};

    return std::string{(std::istreambuf_iterator<char>{s}),
                       std::istreambuf_iterator<char>{}};
}

static GLuint createShader(GLuint type, const std::string& path)
{
    const std::string source = readLines(path);
    const char* c_str = source.c_str();
    GLint success{};

    GLuint shader = glCreateShader(type);

    glShaderSource(shader, 1, &c_str, NULL);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if(!success) {
        GLint infoLogLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar* infoLog = new GLchar[infoLogLength];
        glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);

        std::cout << infoLog << std::endl;
        delete[] infoLog;
    }

    return shader;
}

static GLuint createProgram(GLuint vertexShader, GLuint fragmentShader)
{
    GLint success{};

    GLint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    glValidateProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &success);

    if(!success) {
        GLint infoLogLength{};
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar* infoLog = new GLchar[infoLogLength];
        glGetProgramInfoLog(program, infoLogLength, NULL, infoLog);
        std::cout << infoLog << std::endl;
        delete[] infoLog;
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return program;
}

Shader::Shader(const std::string& vertexPath, const std::string& fragmentPath)
{
    GLuint vertexShader = createShader(GL_VERTEX_SHADER, vertexPath);
    GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, fragmentPath);

    m_program = createProgram(vertexShader, fragmentShader);
}
