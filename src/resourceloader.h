#ifndef RESOURCELOADER_H
#define RESOURCELOADER_H

#include <string>

#include "texture.h"

class ResourceLoader
{
public:
    ResourceLoader(const std::string& resourceDirectory);

    Texture::Ptr loadTexture(const std::string& file) const;

private:
    std::string m_resourceDirectory;
};

#endif // RESOURCELOADER_H
