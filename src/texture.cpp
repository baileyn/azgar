#include "texture.h"

Texture::Texture(unsigned int width, unsigned int height, const std::vector<unsigned char>& data)
    : m_width{width}, m_height{height}, m_data{data}
{
    generate();
}

void Texture::generate()
{
    glGenTextures(1, &m_id);

    glBindTexture(GL_TEXTURE_2D, m_id);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_data.data());
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);
}
