#version 330 core

in vec2 TextureCoords;

out vec4 color;

uniform sampler2D background;

void main() {
    vec4 color1 = texture(background, TextureCoords);
    color = color1;
}
